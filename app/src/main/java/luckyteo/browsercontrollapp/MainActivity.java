package luckyteo.browsercontrollapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Button btnClose = findViewById(R.id.btn_close);
//        btnClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                MainActivity.this.moveTaskToBack(true);
//            }
//        });

        WebView myWebView = findViewById(R.id.webview);
        WebSettings settings = myWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        myWebView.setWebChromeClient(new WebChromeClient());

        myWebView.addJavascriptInterface(new WebAppInterface(this, myWebView), "Android");

        myWebView.loadUrl("http://149.28.140.56/test.html");
//        myWebView.loadUrl("file:///android_asset/code.html");
    }
}
