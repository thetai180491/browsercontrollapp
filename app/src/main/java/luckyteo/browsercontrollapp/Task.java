package luckyteo.browsercontrollapp;

import android.os.AsyncTask;
import android.util.Log;
import android.webkit.WebView;

/**
 * Created by nguyenthetai on 2/12/19.
 */
public class Task extends AsyncTask<String, Void, String> {
    private String TAG = "Task";
    private String callback;
    private WebView webView;

    public Task(WebView webView, String callback) {
        this.callback = callback;
        this.webView = webView;
    }

    @Override
    protected String doInBackground(String... strings) {
        Log.i(TAG, "doInBackground: ");
        return "String sync process: " + strings[0];
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        webView.loadUrl("javascript:" + callback + "('" + s + "')");
    }
}
