package luckyteo.browsercontrollapp;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

/**
 * Created by nguyenthetai on 2/12/19.
 */
public class WebAppInterface {

    Context mContext;
    WebView webView;
    private String TAG = "WebAppInterface";

    /**
     * Instantiate the interface and set the context
     */
    WebAppInterface(Context c) {
        mContext = c;
    }

    public WebAppInterface(MainActivity mainActivity, WebView myWebView) {
        mContext = mainActivity;
        webView = myWebView;
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public String getData(String msg) {
        return "String process: " + msg;
    }


    @JavascriptInterface
    public void getSyncData(String msg, String callback) {
        Log.i(TAG, msg);
        Log.i(TAG, callback);
        Task task = new Task(webView, callback);
        task.execute(msg);
    }
}
